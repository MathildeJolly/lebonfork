<?php


namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @param ProductRepository $repository
     * @Route("/", name="home")
     */
    public function index(ProductRepository $repository)
    {
        $product = $repository->findLatest();

        return $this->render('pages/home.html.twig', [
            'products' => $product
        ]);
    }
}